package ee.bcs.valiit;

/* Byte klassi muutujate väärtused võivad olla ka tavalised numbrid. Lisaks saab kursoriga väärtuse juures
olles ilmuva lambipirnikese abil automaatselt konverteerida väärtuseid binaarseks või muusse süsteemi.
 */

public class Main {

    public static void main(String[] args) {
        byte a = 2, b = 2;
        Byte c = 2, d = 2;
        System.out.println("a + b = " + (a + b));
        System.out.println("c + d = " + (c + d));
        System.out.println("a + d = " + (a + d));
    }
}
